# Slackware-Commander
Slackware Commander

## Install
Download zip file, extract, and run as root 
```
./install.sh
```
### requires
```
gtkdialog
zenity
```
![Slackware-Commander](https://github.com/rizitis/Slackware-Commander/raw/main/Slackware-Commander.png)
#### Dark and white 
![Slackware-Commander1](https://github.com/rizitis/Slackware-Commander/raw/main/Slackware-Commander1.png)
